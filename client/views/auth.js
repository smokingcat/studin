
if (Meteor.isClient) {
  Template.registerSchool.events({
    'submit form': function(event, template){
      event.preventDefault();
      var email = template.find('#register-email').value;
      var password = template.find('#register-password').value;
      var role = "school"
      Router.go('/dashboard');
      Accounts.createUser({email: email, password: password, profile: {role: role, schools: 0}});
    }
  });
  Template.registerStudent.events({
    'submit form': function(event, template){
      event.preventDefault();
      var email = template.find('#register-email').value;
      var schoolId = template.find('.selectpicker').value;
      var schoolName = Schools.find({_id: schoolId}).fetch();
      schoolName = schoolName[0].schoolName;
      var password = template.find('#register-password').value;
      var role = "student";
      Router.go('/dashboard');
      Accounts.createUser({email: email, password: password, profile: {role: role, schoolId: schoolId, schoolName: schoolName}});
    }
  });
  Template.registerCompany.events({
    'submit form': function(event, template){
      event.preventDefault();
      var email = template.find('#register-email').value;
      var password = template.find('#register-password').value;
      var role = "company"
      Router.go('/dashboard');
      Accounts.createUser({email: email, password: password, profile: {role: role, offers: 0}});
    }
  });

  Template.login.events({
    'submit form': function(event, template){
      event.preventDefault();
      var emailVar = template.find('#login-email').value;
      var passwordVar = template.find('#login-password').value;
      Meteor.loginWithPassword(emailVar, passwordVar);
      Router.go('/dashboard');
      console.log("Form submitted.");
    }
  });
  Template.authButtons.events({
    'click #dropStudent': function(event, template){
      Router.go('/student/register');
    },
    'click #dropSchool': function(event, template){
      Router.go('/school/register');
    },
    'click #dropCompany': function(event, template){
      Router.go('/company/register');
    }
  });
  Template.authButtons.rendered = function(){
    $('.ui.dropdown')
    .dropdown();
  },
  /*AUTH VERSION2*/
Template.authConnectSchool.events({
    'submit form': function(event, template){
      event.preventDefault();
      var email = template.find('#register-email').value;
      var password = template.find('#register-password').value;
      var role = "school"
      Router.go('/dashboard');
      Accounts.createUser({email: email, password: password, profile: {role: role, schools: 0}});
    }
  });
  Template.authConnectStudent.events({
    'submit form': function(event, template){
      event.preventDefault();
      var email = template.find('#register-email').value;
      var schoolId = $('.dropdown input').val();
      var schoolName = Schools.find({_id: schoolId}).fetch();
      schoolName = schoolName[0].schoolName;
      var password = template.find('#register-password').value;
      var role = "student";
      Router.go('/dashboard');
      Accounts.createUser({email: email, password: password, profile: {role: role, schoolId: schoolId, schoolName: schoolName}});
    }
  });
  Template.authConnectCorporate.events({
    'submit form': function(event, template){
      event.preventDefault();
      var email = template.find('#register-email').value;
      var password = template.find('#register-password').value;
      var role = "company"
      Router.go('/dashboard');
      Accounts.createUser({email: email, password: password, profile: {role: role, offers: 0}});
    }
  });

  Template.authLogin.events({
    'submit form': function(event, template){
      event.preventDefault();
      var emailVar = template.find('#login-email').value;
      var passwordVar = template.find('#login-password').value;
      Meteor.loginWithPassword(emailVar, passwordVar);
      Router.go('/dashboard');
      console.log("Form submitted.");
    }
  });

}

if (Meteor.isServer) {
  Accounts.onCreateUser(function(options, user) {
    console.log(user._id);
    console.log(options.profile.role);
    schoolName : options.profile.schoolName;
    user.profile = options.profile;
    return user;
  });
}
