if (Meteor.isClient) {
    Session.setDefault("connect-mode", "log-in");
    $(function() {
  		$(".log-in").focus();
	});
    Template.register.events({
    	'click .register': function(){
            $("*").removeClass('active');
            $(".register").addClass('active');
        	Session.set("connect-mode", "register");
        }
    });
        Template.login.events({
        'click .log-in': function(){
            $("*").removeClass('active');
            $(".log-in").addClass('active');
        	Session.set("connect-mode", "log-in");
        }
    });

}
