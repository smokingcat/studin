Template.auth.rendered = function(){
}

Template.authLogin.rendered = function(){
	$(".auth-top-menu-log").css("background-color","#616161");
	$(".auth-top-menu-reg").css("background-color","#424242");
	$("#login").css("background-color","#616161");
}

Template.authConnectSubMenu.rendered = function(){
	$(".auth-top-menu-log").css("background-color","#424242");
	$(".auth-top-menu-reg").css("background-color","#616161");
	$(".auth-connect-sub-menu-st").css("background-color","#616161");
	$(".auth-connect-sub-menu-sc").css("background-color","#616161");
	$(".auth-connect-sub-menu-co").css("background-color","#616161");
}

Template.authConnectStudent.rendered = function() {
	$('.ui.dropdown').dropdown();
	$(".auth-connect-sub-menu-st").css("background-color","#616161");
	$(".auth-connect-sub-menu-sc").css("background-color","#424242");
	$(".auth-connect-sub-menu-co").css("background-color","#424242");
	$(".auth-two").css("background-color","#616161");
}


Template.authConnectSchool.rendered = function() {
	$(".auth-connect-sub-menu-sc").css("background-color","#616161");
	$(".auth-connect-sub-menu-st").css("background-color","#424242");
	$(".auth-connect-sub-menu-co").css("background-color","#424242");
	$(".auth-two").css("background-color","#616161");
}


Template.authConnectCorporate.rendered = function() {
	$(".auth-connect-sub-menu-co").css("background-color","#616161");
	$(".auth-connect-sub-menu-sc").css("background-color","#424242");
	$(".auth-connect-sub-menu-st").css("background-color","#424242");
	$(".auth-two").css("background-color","#616161");
}

Template.authConnectStudent.helpers({
	schools: function(){
		return Schools.find().fetch();
	}
});
