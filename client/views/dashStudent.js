if(Meteor.isClient){
  Template.registerStudent.helpers({
    schools: function() {
      return Schools.find();
    }
  });
  Template.addChannels.helpers({
    channels: function() {
      return Channels.find({userId: Meteor.user()._id});
    }
  });
	Template.feedChannel.helpers({
		offers: function() {
			return Template.currentData();
		},
		isStudent: function(){
			if(Meteor.user().profile.role === "student")
				return true;
			else
				return false;
		}
	});

}
