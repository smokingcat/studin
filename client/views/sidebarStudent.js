if (Meteor.isClient) {
    Template.sidebarStudent.events({
        'click .sidebar-toggle': function() {
        $("#wrapper").toggleClass("toggled");
        $(".logo-school").toggle("slow");
    	},
        'click #logout': function(){
        	Meteor.logout();
          Router.go("/login");
        },
    });
    Template.sidebarStudent.helpers({
      'logo': function(){
        var school = Schools.find({_id: Meteor.user().profile.schoolId}).fetch();
        return school[0].logo
      }
    });
 /*   	$("[data-role=dropdown]").dropdown();
    })
*/
}
