if (Meteor.isClient) {
    Template.sidebarCompany.events({
        'click .sidebar-toggle': function() {
        $("#wrapper").toggleClass("toggled");
        $(".logo-school").toggle("slow");
    	},
        'click #pushPost': function(){
    		Session.set("page", "pushPost");
    	},
        'click #logout': function(){
        	Meteor.logout();
          Router.go("/auth/login");
        },
        'click #home': function(){
          Session.set("page", "offers");
        },
        'click #offers': function(){
          Session.set("page", "offers");
        },
        'click .logo-school': function(){
          Router.go("/", this);
        }
    });
    Template.firstTimer.events({
      'click #go': function(){
        Session.set("page", "pushPost");
	}
});
}
