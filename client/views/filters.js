Template.filters.events({
	'click #cdd': function(){
		$('.offer-CDD').toggle();
		$('#cdd').toggleClass('a-blue');
	},
	'click #stage': function(){
		$('.offer-Stage').toggle();
		$('#stage').toggleClass('a-blue');
	},
	'click #cdi': function(){
		$('.offer-CDI').toggle();
		$('#cdi').toggleClass('a-blue');
	},
	'click #vie': function(){
		$('.offer-VIE').toggle();
		$('#vie').toggleClass('a-blue');
	},
});

Template.filters.rendered = function(){
		$('#cdd').toggleClass('a-blue');
		$('#stage').toggleClass('a-blue');
		$('#cdi').toggleClass('a-blue');
		$('#vie').toggleClass('a-blue');
}
