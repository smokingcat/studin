if (Meteor.isClient) {
    Template.sidebarSchool.events({
        'click .sidebar-toggle': function() {
        $("#wrapper").toggleClass("toggled");
        $(".logo-school").toggle("slow");
      },
        'click #logout': function(){
          Meteor.logout();
          Router.go("/login");
        },
        'click .logo-school': function(){
          Router.go("/");
        }
    });
	Template.sidebarSchool.helpers({
		channels: function() {
			return Channels.find({userId: Meteor.user()._id}).fetch();
		}
	});
	Template.sidebarStudent.helpers({
		channels: function() {
			return Channels.find({belongsToSchoolId: Meteor.user().profile.schoolId}).fetch();
		}
	});
}
