if (Meteor.isClient) {
	Template.dashboard.events({
		'click .logout': function(event){
			Meteor.logout();
		}
	});
	Template.feedStudent.helpers({
		offers: function() {
			return Offers.find();
		}
	});
	Template.feedCompany.helpers({
		offers: function() {
			return Offers.find({submitId: Meteor.user()._id}).fetch();
		},
		noOffers: function() {
			if (Offers.find({submitId: Meteor.user()._id}).count())
				return false;
			else
				return true;
		}
	});
	Template.pushPost.events({
		'submit form': function(event, template){
			var name = template.find('#name').value;
			var corps = $('#corps').html();;
			var salary = template.find('#salary').value;
			var email = template.find('#email').value;
			var jobType = $('input[name="jobType"]:checked').val();
			var belongsToChannels = [];
			$('.selectpicker :selected').each(function(i, selected){
  				belongsToChannels[i] = $(selected).text();
			});
			var channels = [];
			channels = $('#channels').val();
			var submitId = Meteor.user()._id;
			Offers.insert({
				name: name,
				submitId: submitId,
				corps: corps,
				salary: salary,
				email: email,
				jobType: jobType,
				belongsToChannels: belongsToChannels,
				channels: channels,
			});
			Meteor.users.update({_id:Meteor.user()._id}, {$set:{"profile.offers": 1}})
			Router.current().render().data();
		}
	});
	Template.pushPost.helpers({
		channels: function() {
			return Channels.find().fetch();
		}
	});
	Template.pushPost.rendered = function(e){
		 $('.selectpicker').selectpicker();
		$('.bootstrap-select').css('width', '100%');
	},
	Template.offer.rendered = function(e){
		  $(".offer-corps").hide();
	},
	Template.dashCompany.rendered = function(e){
			Session.set("page", "offers");
	},
	Template.offer.events({
		'click .offer': function(event){
			$(event.target).children(".offer-corps").toggle("fast");
		},
		'click .offer-corps': function(event){
			$(event.target).parents('offer-corps').toggle("fast");
		}
	});
}
