if(Meteor.isClient){
  Template.createSchool.events({
        'submit form': function(){
          Meteor.users.update({_id:Meteor.user()._id}, {$set:{"profile.schools": 1}});
          Router.go("/create-school-thanks");
        }
  });
  Template.sidebarSchool.helpers({
    'logo': function(){
      var school = Schools.find({createdBy: Meteor.user()._id}).fetch();
      return school[0].logo
    }
  });
  Template.updateSchool.events({
    'submit form': function(){
      Router.go("/update-school-thanks");
    }
  });
}
