Router.configure({ layoutTemplate: 'hello' });


Router.map(function () {
});

Router.route('/dashboard', function (){
		if (Meteor.user().profile.role === 'student') {
				this.redirect('/student');
		}
		else if (Meteor.user().profile.role === 'company') {
				this.redirect('/company');
		}
		else if (Meteor.user().profile.role === 'school') {
				this.redirect('/school');
		}
});

Router.route('/', function (){
		if (Meteor.userId()){
				this.redirect('/dashboard');
		}
		else {
				this.redirect('/auth/login');
		}
});

Router.route('/login', function () {
		this.render('login');
});

Router.route('/school/register', function () {
		this.render('registerSchool');
});

Router.route('/company/register', function () {
		this.render('registerCompany');
});

Router.route('/student/register', function () {
		this.render('registerStudent');
});

Router.route('/student', function () {
		this.render('dashStudent');
});

Router.route('/create-school-thanks', function () {
		this.render('createSchoolThanks', {to: 'aside'});
});

Router.route('/update-school-thanks', function () {
		this.render('updateSchoolThanks', {to: 'aside'});
});


Router.route('/update-school', function () {
		this.render('updateSchool', {
				to: 'aside',
				data: function(){
						school = Schools.find({createdBy: Meteor.user()._id}).fetch();
						return school[0];
				}
		});
});

Router.route('/channels/:_id', function () {
  var params = this.params;
  this.render('feedChannel', {
	data: function() {
		var offers = Offers.find( { channels: { $in: [ params._id ] } } ).fetch();
		return offers;
	}
  });
});

Router.route('/add-channels', function () {
		this.render('addChannels', {
				to: 'aside',
				data: function(){
						school = Schools.find({createdBy: Meteor.user()._id}).fetch();
						return school[0];
				}
		});
});
Router.route('/school', function () {
		if (Meteor.user().profile.schools === 0)
				this.render('createSchool', {to: 'aside'});
		this.render('dashSchool');
});

Router.route('/company', function () {
  this.render('dashCompany');
  if (Session.equals("page", "pushPost"))
  {
  	this.render('pushPost', {to: 'aside'});
  }
  else if(Session.equals("page", "offers"))
  {
    this.render('offers', {to: 'aside'});
  }
  else if(Session.equals("page", "welcome"))
  {
	this.render('firstTimer', {to: 'aside'});
}
});

/* auth v2  */
Router.route('/auth', function () {
		this.render('auth');
});

Router.route('/auth/login', function () {
	this.render('auth');
	this.render('authLogin', {to: 'one'});
	this.render('void', {to: 'two'});
});

Router.route('/auth/register', function () {
	this.render('auth');
	this.render('auth-connect-sub-menu', {to: 'one'});
	this.render('void', {to: 'two'});
});

Router.route('/auth/register/student', function () {
	this.render('auth');
	this.render('auth-connect-sub-menu', {to: 'one'});
	this.render('auth-connect-student', {to: 'two'});
});

Router.route('/auth/register/school', function () {
	this.render('auth');
	this.render('auth-connect-sub-menu', {to: 'one'});
	this.render('auth-connect-school', {to: 'two'});
});

Router.route('/auth/register/corporate', function () {
	this.render('auth');
	this.render('auth-connect-sub-menu', {to: 'one'});
	this.render('auth-connect-corporate', {to: 'two'});
});
