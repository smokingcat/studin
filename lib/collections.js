Offers = new Mongo.Collection('offers');


Schools = new Mongo.Collection("schools");
Schools.attachSchema(new SimpleSchema({
  schoolName: {
    type: String,
    optional: false,
    label: "Nom de l'établissement:",
    max: 200
  },
  createdBy: {
    type: String,
    autoValue:function(){ return this.userId }
  },
  logo: {
    type: String,
    optional: true,
    label: "Logo (url au format .png):",
    max: 200
  },
  adresse: {
    type: String,
    optional: true,
    label: "Adresse de l'établissement:",
    max: 200
  }
}));

Channels = new Mongo.Collection("channels");
Channels.attachSchema(new SimpleSchema({
  name: {
    type: String,
    optional: false,
    label: "Nom de la formation",
    max: 200
  },
  description: {
    type: String,
    optional: false,
    label: "Description",
    max: 200
  },
  createdBy: {
    type: String,
    autoValue:function(){
      var mySchool =  Schools.find({createdBy: Meteor.user()._id}).fetch();
      mySchool = mySchool[0];
      var mySchoolId = mySchool._id;
      return mySchoolId;
    }
  },
  userId: {
      type: String,
      autoValue:function(){
        return Meteor.user()._id;
      }
    },
 belongsToSchool: {
	type: String,
	autoValue:function(){
		return Schools.find({createdBy: Meteor.user()._id}).fetch()[0].schoolName
	}
 },
 belongsToSchoolId: {
	type: String,
	autoValue:function(){
		return Schools.find({createdBy: Meteor.user()._id}).fetch()[0]._id
	}
 },
  }
));
