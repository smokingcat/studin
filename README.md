# Studin   
----
### Context ###

Studin is the outcome of a three weeks program (hackathon style) in close partnership with HEC Paris and design school Condé.

We had to use to our advantage the principles and unique process of the [_Design Thinking_](http://frenchweb.fr/le-design-thinking-un-nouvel-avantage-competitif/122936) methodology to tackle a broad and complex issue: job seeking.

Three weeks to create a service that brings innovation while being technically feasible, that solves a real user issue and is ready for market.

_We did [blog](http://dt-g2.tumblr.com/) about it._

### Design thinking ? ###
![](http://i.imgur.com/4V8wPXs.jpg?1)

In short, design thinking is a framework to generate innovative and human centered propositions of value.

### The idea ###
The idea was to make a platorm that would replace the job board for students vastly present in most schools websites.

For the record, in 2015 it looks like this: 

![](http://i.imgur.com/fvvUMRr.png)

We identified many problems for the schools, the students, and the companies looking for fresh brains.

* Companies lose a lot of time to reach their audience with complex forms specific to each schools
* They have a bad visibility of the higher education market
* Students often struggle with bad UIs, irrelevant or outdated offers
* Schools pays a lot of money for bad designed tools yet crucial in their mission to place students in the job market

Our idea was to make an efficient and nice looking platform that would satisfy the three protagonists.

The app would be free for the schools, this enabeling us to have a fast growing pool of users.

Because the schools would have to provide datas on their different trainings, and students be registered in their respective classes,

Our added value for the companies would then reside in our knowledge of the higher education mapping, 

enabeling them to reach directly the correct audience, and stop having to deal with irrelevant applications.

As such: 

![](http://i.imgur.com/sFwozpR.png)


### My role ###
I was the lead developper in a 6 people team, I took advantage of it to sharpen my web apps development skills.

We used the framework [Meteor.js](https://www.meteor.com/) because we tought it was the most relevant to get a prototype running as fast as possible.

In the meantime MongoDB offered us a lot of flexibility to iterate on the information achitecture part of the project.

I had to create the app from the ground, and take it all the way to the internet where it now [lives](http://freshbrains.herokuapp.com).


### What we learned ###

* Collaborate in a group and synchronise our work force, being people with very different backgrounds 
* Try not to make false asumptions and always confront our ideas to the people that will use them in the end 
* Discover inovative workflows, new ways of doing things
* Bootstrap and test ideas fast.

